# PW Store app
    Toy portable and 0 dependencies (Python only) password and small files encrypted storage CLI.


# Dev
    Download
    Simply run python __main__.py
    Or python .


# Installation

    - Option 1:
        Download all
        Zip all contents, remove extension, and distribute with some name (i.e., pw_store)
        Simply run python pw_store standing on the file location


    - Option 2 (advanced):
        Download all
        Standing on the folder: pip install .
            It will get installed as a python library and executable
        simply execute as: pw_store

        running `pw_store` will use a database file on the current working directory,
        set PW_STORE_DB_FILE env variable to use a fixed db file

    - Option 3 (standalone, no python required):
        Download all
        Install pyinstaller: pip install pyinstaller
        Run: pyinstaller --onefile --paths=./pw_store/vendor --name=pw_store __main__.py
            -- Ideally this should be run inside a virtualenv so it doesn't pick wrong imports from the global scope.
            -- virtualenv ./env (or virtualenv3)
            -- (Windows) .\env\Scripts\activate.bat
            -- (OSX) source ./bin/activate
