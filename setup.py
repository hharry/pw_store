import re
import ast

from setuptools import setup, find_packages


# Get version
_version_re = re.compile(r'__version__\s+=\s+(.*)')


with open('pw_store/__init__.py', 'rb') as f:
    version = str(ast.literal_eval(_version_re.search(f.read().decode('utf-8')).group(1)))


# https://packaging.python.org/tutorials/distributing-packages/

setup(
    name='pw_store',
    author='Cristiano Coelho',
    author_email='cristianocca@hotmail.com',
    version=version,
    description='Toy portable and 0 dependencies (Python only) password and small files encrypted storage CLI.',
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'pw_store=pw_store:cli',
        ],
    }
)
