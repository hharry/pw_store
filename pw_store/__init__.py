import os
import sys
import hashlib
import hmac

import random
from string import ascii_lowercase as lowercase
from string import ascii_uppercase as uppercase
from string import digits
from string import punctuation

import sqlite3
from datetime import datetime


# Add vendor directory to module search path
parent_dir = os.path.abspath(os.path.dirname(__file__))
vendor_dir = os.path.join(parent_dir, 'vendor')

sys.path.insert(0, vendor_dir)

# Vendored modules
# pylint: disable=wrong-import-position, redefined-builtin, invalid-name
from .vendor import pyaes
from .vendor import click



__version__ = '0.1.0'

'''
Pure Python implementation with no external dependencies (no venv required nor pip install)
Requires Python >= 3.7


Thanks to click for the CLI library: https://click.palletsprojects.com/en/8.0.x/
Thanks to pyaes for the pure python AES implementation: https://github.com/ricmoo/pyaes
'''


# ------------------------------------------------------------------------------
# ----------------------------------- Constants --------------------------------
# ------------------------------------------------------------------------------

# TODO: Implement Tests


# For DB file see cli definition (def cli).
# Env variable: PW_STORE_DB_FILE


# Pagination pauses
PAGE_SIZE = 15

# Can be increased to further defend against brute force attacks, although not necessary
# Increasing this, greatly increases processing time to encrypt and decrypt data.
DERIVATION_ITERS = 120000
SALT_LENGTH = 16    # 128 bits, beyond this is almost useless

# DO NOT CHANGE THIS, encryption won't work with different length
# Will also leave existing data unreadable.
KEY_LENGTH = 32

BUFFER_SIZE = 1024*10


LOWER_AND_DIGIT = lowercase + digits
LEVEL_LABELS = {
    1: ('Bad', 'red'),
    2: ('Low', 'magenta'),
    3: ('Good', 'yellow'),
    4: ('Excellent', 'green')
}


sys_random = random.SystemRandom()
digest_module = hashlib.sha512


# ------------------------------------------------------------------------------
# ----------------------------------- Utils ------------------------------------
# ------------------------------------------------------------------------------


def force_bytes(v, enc='utf-8'):
    '''
    v: str, bytes, or bytearray
    '''
    if isinstance(v, (bytes, bytearray)):
        return v

    if isinstance(v, str):
        return v.encode(enc)

    # handles memoryview and others
    return bytes(v)


def force_str(v, enc='utf-8'):
    '''
    v: str or bytes
    '''
    return v if isinstance(v, str) else v.decode(enc)


def validate_key_types(d_key, s_key):
    '''
    Helper to ensure keys are binary
    '''

    if not isinstance(d_key, bytes):
        raise ValueError("Data key is not bytes.")

    if not isinstance(s_key, bytes):
        raise ValueError("Signature key is not bytes.")

    return True


def get_salt(l):
    '''
    Returns a randomly generated salt of l bytes hex encoded
    '''
    return os.urandom(l)


def get_derived_key(key, salt, l, iters):
    '''
    Returns a derived key (binary) from a supplied key and salt.

    Derived keys helps to protect against brute force and reverse lookup tables
    of the original key while also being able to generate new unique keys each time
    if the salt is changed.

    key: original key/password

    salt: randomly generated salt

    l: length of the returned derived key

    iters: iterations.

    '''

    return hashlib.pbkdf2_hmac(digest_module().name, force_bytes(key), force_bytes(salt), iters, l)



# We are going to use a hmac to authenticate the ciphertext as well


def encrypt(d_key, s_key, data):
    '''
    Encrpyts the given data.

    d_key: 32 bytes binary (bytes) key for encryption.
        Should be unique (not reused) for each encryption in order to keep
        AES CTR secure.

    s_key: 32 bytes binary (bytes) for signing

    data: bytes or unicode (will be converted to bytes as utf-8) data, or a file-like object
        (with a read method, must be binary/bytes read or decryption will be inconsistent)

    returns: (encrypted bytes data, bytes hmac of the encrypted data)
    '''

    validate_key_types(d_key, s_key)

    # Reusing the same IV should be fine since
    # each d_key is different, due to the key derivation
    # used and a random salt for each derivation
    aes = pyaes.AESModeOfOperationCTR(d_key)


    # If we got a stream, process by chunks
    # To avoid destroying the memory.
    # although final encrypted data will be in memory, it will only be 1 copy

    if hasattr(data, 'read'):

        hm = hmac.new(s_key, digestmod=digest_module)
        def gen():
            part = data.read(BUFFER_SIZE)

            while part:
                enc = aes.encrypt(part)
                hm.update(enc)
                yield enc

                part = data.read(BUFFER_SIZE)

        return b''.join(v for v in gen()), hm.digest()

    else:
        res = aes.encrypt(force_bytes(data))
        return res, hmac.new(s_key, res, digest_module).digest()


def decrypt(d_key, s_key, data):
    '''
    Decrypts the given enrypted data.

    d_key: 32 bytes binary (bytes) key for decryption

    s_key: 32 bytes binary (bytes) key for signing

    data: bytes encrypted data, NOT a file like object.

    returns: (decrypted bytes data, bytes hmac of the encrypted data)

    NOTE: result is bytes data even if it was originally unicode, handle with care.
    '''

    validate_key_types(d_key, s_key)

    aes = pyaes.AESModeOfOperationCTR(d_key)


    data_l = len(data)

    # If data is bigger than our buffer, process by chunks
    if data_l > BUFFER_SIZE:

        hm = hmac.new(s_key, digestmod=digest_module)

        def gen():
            i = 0
            while i < data_l:
                part = data[i:i+BUFFER_SIZE]
                hm.update(part)
                yield aes.decrypt(part)
                i += BUFFER_SIZE

        return b''.join(v for v in gen()), hm.digest()

    else:
        res = aes.decrypt(data)
        return res, hmac.new(s_key, data, digest_module).digest()


# ------------------------------------------------------------------------------
# ----------------------------------- DB ------------------------------------
# ------------------------------------------------------------------------------

class DB(object):

    TEXT_TYPE = 'text'
    FILE_TYPE = 'file'
    BINARY_COLS = ('d_salt', 's_salt', 'encrypted', 'signature')

    def __init__(self, db_file, auto_commit=True):
        self.con = sqlite3.connect(db_file)
        self.auto_commit = auto_commit

    def dispose(self):
        self.con.close()


    def commit(self):
        self.con.commit()


    def rollback(self):
        self.con.rollback()


    def init_db(self):
        '''
        Inits DB structure. Read comments for columns info.
        '''

        # type: data type ('text' / 'file')
        # d_salt: data salt
        # s_salt: signature salt
        # updated: should be stored as ISO dates

        self.con.execute('''CREATE TABLE IF NOT EXISTS data (
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            name TEXT,
            type TEXT,
            updated TEXT,
            d_salt BLOB,
            s_salt BLOB,
            iters INT,
            encrypted BLOB,
            signature BLOB
        );
        ''')


    def add(self, name, type, d_salt, s_salt, iters, encrypted, signature):
        '''
        Adds a new entry.
        '''

        query = 'INSERT INTO data values(null, ?, ?, ?, ?, ?, ?, ?, ?)'

        # Careful with binary types
        args = [
            name,
            type,
            datetime.now().isoformat(),
            sqlite3.Binary(d_salt),
            sqlite3.Binary(s_salt),
            iters,
            sqlite3.Binary(encrypted),
            sqlite3.Binary(signature)
        ]

        self.con.execute(query, args)

        if self.auto_commit:
            self.commit()


    def update(self, id, name, type, d_salt, s_salt, iters, encrypted, signature):
        '''
        Updates an existing entry by id overwriting all values.
        '''
        query = 'UPDATE DATA set name=?, type=?, updated=?, d_salt=?, s_salt=?, iters=?, encrypted=?, signature=? WHERE id = ?'

        # Careful with binary types
        args = [
            name,
            type,
            datetime.now().isoformat(),
            sqlite3.Binary(d_salt),
            sqlite3.Binary(s_salt),
            iters,
            sqlite3.Binary(encrypted),
            sqlite3.Binary(signature),
            id
        ]

        self.con.execute(query, args)

        if self.auto_commit:
            self.commit()


    def delete(self, id):
        '''
        Deletes an existing entry by id. Nothing happens if not found
        '''
        self.con.execute('DELETE FROM data WHERE id = ?', [id])
        if self.auto_commit:
            self.commit()


    def search(self, name=None, limit=PAGE_SIZE, offset=0, order_by='-id'):
        '''
        Searches all data by name, if None returns all.

        limit: result count limit
        offset: offset
        order_by: possible values:
            [-][id/name/updated]

        Returns a list of [{id, name, type, updated}] values
        '''

        if limit < 0:
            raise ValueError("Invalid limit.")

        if offset < 0:
            raise ValueError("Invalid offset.")

        if not order_by:
            order_by = '-id'

        asc = 'asc'
        col = ''
        if order_by[0] == '-':
            asc = 'desc'
            col = order_by[1:]
        else:
            col = order_by

        if col not in {'id', 'name', 'updated'}:
            raise ValueError("Invalid order_by.")


        # Dynamic SQL, it's ok we validated the values
        query = 'SELECT id, name, type, updated FROM data'
        args = []

        if name:
            query += " WHERE name like ?"
            args.append('%' + name + '%')

        query += ' ORDER BY %s %s' % (col, asc)

        if limit:
            query += ' LIMIT %s' % limit
            if offset:
                query += ' OFFSET %s' % offset


        return [{
            'id': v[0],
            'name': v[1],
            'type': v[2],
            'updated': v[3]
        } for v in self.con.execute(query, args)]


    def get(self, id, cols=None):
        '''
        Returns all data by id or None if it doesn't exist.

        cols: None for all data or a list/tuple for specific cols
            Note: Not safe, could lead to sql injection
        '''

        if not cols:
            cols = (
                'id',
                'name',
                'type',
                'updated',
                'd_salt',
                's_salt',
                'iters',
                'encrypted',
                'signature'
            )

        r = list(self.con.execute('SELECT %s FROM data WHERE id=?' % ','.join(cols), [id]))
        if not r:
            return None

        r = r[0]

        res = dict(zip(cols, r))

        for b in self.BINARY_COLS:
            if b in res:
                res[b] = bytes(res[b])

        return res


    def wipe(self):
        '''
        Deletes everything from the database.
        '''
        self.con.execute('DELETE FROM data')
        if self.auto_commit:
            self.commit()



# ------------------------------------------------------------------------------
# ----------------------------------- Commands ---------------------------------
# ------------------------------------------------------------------------------

def print_version(ctx, param, value):
    if not value or ctx.resilient_parsing:
        return
    click.echo(__version__)
    ctx.exit()


@click.group()
@click.option('--version', is_flag=True, callback=print_version, expose_value=False, is_eager=True, help='Version info.')
@click.option(
    '--db',
    type=click.Path(exists=False, dir_okay=False, resolve_path=True, file_okay=True),
    default=os.environ.get('PW_STORE_DB_FILE', 'pw_store.db'),
    help='Optional path to store the database. Loaded from PW_STORE_DB_FILE or a fixed default.'
)
@click.pass_context
def cli(ctx, db):
    '''
    Password and small files storage Application, cross platform with only Python (2 or 3) as a required dependency.

    Stores passwords or small files using a single (or multiple) master password, which is used to encrypt
    sensitive data before being stored into a local, shareable database that can be accessed cross platform
    or even through a network, each record is stored with either always the same password, or separate passwords.

    Master password (or passwords) will be prompted each time it is required, mainly to store and retrieve data.

    NOTE: Not all data, such as friendly names, is encrypted. Do not write sensitive data in names and labels.

    Pass --db FILE_PATH to specify a custom db file (path must exist), or set it through the PW_STORE_DB_FILE env variable
    to be loaded automatically.

    Run COMMAND --help to get help for each specific command.

    Use double quotes (") to wrap parameters that might need it (such as params with spaces) instead of single quotes.
    '''
    click.echo('Running version %s' % __version__)
    click.echo('Using DB: %s' % db)
    click.echo('')

    # Initialize context object and db object
    ctx.obj = {}
    ctx.obj['db'] = DB(db)

    # Make sure tables exist
    ctx.obj['db'].init_db()


@cli.command('make_password', short_help='Makes a new random password')
@click.argument('length', default=8, type=int)
@click.argument('complexity', default=4, type=click.IntRange(1, 4))
@click.pass_context
def make_password(ctx, length, complexity, output=True):
    '''
    Generate a random password with given length

    Allowed complexity levels:

    Complexity 1: only lowercase chars

    Complexity 2: Previous level plus at least 1 digit

    Complexity 3: Previous levels plus at least 1 uppercase char

    Complexity 4: Previous levels plus at least 1 punctuation char


    Complexity > 1 requires at least length 8
    '''

    # Validations
    if length < 1:
        raise click.ClickException("Invalid Length.")

    if length < 8 and complexity in {4, 3, 2}:
        raise click.ClickException("Invalid Length and complexity combination.")


    # Base complexity and alfabet
    base = sys_random.choice(lowercase)
    alfabet = lowercase


    # First ensure requirements

    if complexity >= 4:
        # Need at least 3 characters
        if length < 3:
            raise click.ClickException("Invalid length and complexity combination.")

        base += sys_random.choice(punctuation)
        alfabet += punctuation

    if complexity >= 3:
        # Need at least 2 characters
        if length < 2:
            raise click.ClickException("Invalid length and complexity combination.")

        base += sys_random.choice(uppercase)
        alfabet += uppercase

    if complexity >= 2:
        base += sys_random.choice(digits)
        alfabet += digits

    # Then, fill up with random alfabet chars
    res = list(base + ''.join(sys_random.choice(alfabet) for _ in range(length - len(base))))

    # randomize again so we don't necessarily have core requirements in front
    sys_random.shuffle(res)

    res = ''.join(res)

    if output:
        click.echo(res)

    return res


@cli.command('check_password', short_help='Checks the password complexity level')
@click.option('--password', prompt='Password', hide_input=True)
@click.pass_context
def check_password(ctx, password, output=True):
    '''
    Checks the password complexity level

    Result levels:

    Complexity 1: If password has only lowercase chars or length < 8

    Complexity 2: Previous level condition and at least 1 digit

    Complexity 3: Previous levels condition and at least 1 uppercase char or 1 punctuation

    Complexity 4: Previous levels condition and at least 1 punctuation
    '''

    if not password:
        raise click.ClickException("Invalid password.")

    # will only reach here if not a level exception
    # Might not be as efficient as a single for loop
    # Might also convert strings to constant sets to avoid overhead

    complexity4 = any(e in punctuation for e in password)
    complexity3 = any(e in uppercase for e in password)
    complexity2 = any(e in digits for e in password)
    complexity1 = any(e in lowercase for e in password)

    if len(password) < 8:
        res = 1

    elif complexity4 and complexity3 and complexity2 and complexity1:
        res = 4

    elif complexity3 and complexity2 and complexity1 or complexity4 and complexity2 and complexity1:
        res = 3

    elif complexity2 and complexity1:
        res = 2

    else:
        res = 1

    if output:
        click.echo(click.style('Level: %s (%s)' % (res, LEVEL_LABELS[res][0]), fg=LEVEL_LABELS[res][1]))

    return res


@cli.command('add', short_help='Adds or updates a password')
@click.argument('name')
@click.argument('id', type=int, default=0)
@click.option('--password', prompt='Password', hide_input=True)
@click.option('--key', prompt='Master Key', hide_input=True, confirmation_prompt=True)
@click.pass_context
def add(ctx, name, id, password, key):
    '''
    Adds or updates a password.

    NOTE: Do not store sensitive data in the name field as this data is readable by anyone.

    If ID is provided, existing ID is updated instead.
    '''

    d_salt = get_salt(SALT_LENGTH)
    s_salt = get_salt(SALT_LENGTH)

    encrypted, hashed = encrypt(
        get_derived_key(key, d_salt, KEY_LENGTH, DERIVATION_ITERS),
        get_derived_key(key, s_salt, KEY_LENGTH, DERIVATION_ITERS),
        password
    )

    db = ctx.obj['db']

    if id:
        db.update(id, name, db.TEXT_TYPE, d_salt, s_salt, DERIVATION_ITERS, encrypted, hashed)
    else:
        db.add(name, db.TEXT_TYPE, d_salt, s_salt, DERIVATION_ITERS, encrypted, hashed)


    # Gotta store:
    # d_salt, s_salt, iters

    # click.echo('%s %s / %s %s' % (repr(d_salt), repr(s_salt), repr(encrypted), repr(hashed)))

    # click.echo("Testing decryption")

    # decrypted, d_hash = decrypt(
    #     get_derived_key(key, d_salt, KEY_LENGTH, DERIVATION_ITERS),
    #     get_derived_key(key, s_salt, KEY_LENGTH, DERIVATION_ITERS),
    #     encrypted
    # )

    # click.echo(password)
    # click.echo(decrypted.decode('utf-8'))
    # click.echo("Signature match? %s" % (hashed == d_hash))


@cli.command('add_file', short_help='Adds or updates a file')
@click.argument('file', type=click.File('rb'))
@click.argument('id', type=int, default=0)
@click.option('--key', prompt='Master Key', hide_input=True, confirmation_prompt=True)
@click.pass_context
def add_file(ctx, file, id, key):
    '''
    Adds or updates a file.

    WARNING: File encryption might (and will) be slow due to pure python implementation.
    Takes about 1 minute for up to 10mb files, use at your own risk.

    WARNING: Files are loaded in memory and massive files won't be supported / or crash based
    on the system available memory.

    If ID is provided, existing ID is updated instead.
    '''
    name = file.name


    d_salt = get_salt(SALT_LENGTH)
    s_salt = get_salt(SALT_LENGTH)

    click.echo("Reading and encrypting data... This might take a while.")
    encrypted, hashed = encrypt(
        get_derived_key(key, d_salt, KEY_LENGTH, DERIVATION_ITERS),
        get_derived_key(key, s_salt, KEY_LENGTH, DERIVATION_ITERS),
        file
    )

    db = ctx.obj['db']

    if id:
        db.update(id, name, db.FILE_TYPE, d_salt, s_salt, DERIVATION_ITERS, encrypted, hashed)
    else:
        db.add(name, db.FILE_TYPE, d_salt, s_salt, DERIVATION_ITERS, encrypted, hashed)

    # click.echo('%s' % repr(hashed))

    # click.echo("Testing decryption")

    # decrypted, d_hash = decrypt(
    #     get_derived_key(key, d_salt, KEY_LENGTH, DERIVATION_ITERS),
    #     get_derived_key(key, s_salt, KEY_LENGTH, DERIVATION_ITERS),
    #     encrypted
    # )

    # #click.echo(decrypted)
    # click.echo("Signature match? %s" % (hashed == d_hash))


@cli.command('get', short_help='Gets a password or file by ID')
@click.argument('id', type=int)
@click.option('--key', prompt='Master Key', hide_input=True)
@click.option('--out', type=click.Path(exists=True, resolve_path=True, file_okay=False), default='./')
@click.pass_context
def get(ctx, id, key, out):
    '''
    Gets a password or file by ID.

    If --out argument is not given, files will be stored with the same name on the current folder.
    '''

    db = ctx.obj['db']
    res = db.get(id)

    if not res:
        raise click.ClickException("Stored value with ID %s not found." % id)


    # decrypt
    decrypted, d_hash = decrypt(
        get_derived_key(key, res['d_salt'], KEY_LENGTH, res['iters']),
        get_derived_key(key, res['s_salt'], KEY_LENGTH, res['iters']),
        res['encrypted']
    )

    if d_hash != res['signature']:
        raise click.ClickException("Signature mismatch, key is wrong or data was altered")

    # if type is text, write to stdout
    if res['type'] == db.TEXT_TYPE:
        click.echo('-' * 10)
        click.echo("ID: %s" % res['id'])
        click.echo("Name: %s" % res['name'])
        click.echo("Updated: %s" % res['updated'])
        click.echo("Decrypted value: %s" % decrypted.decode('utf-8'))
        click.echo('-' * 10)

    else:
        if not out:
            out = './'

        fname = res['name']
        target_file = os.path.join(out, fname)

        with open(target_file, 'wb') as f:
            f.write(decrypted)

        click.echo('-' * 10)
        click.echo("File written to: %s" % target_file)
        click.echo('-' * 10)




@cli.command('delete', short_help='Deletes a password by its ID')
@click.argument('id', type=int)
@click.pass_context
def delete(ctx, id):
    '''
    Deletes a password by ID
    '''

    res = ctx.obj['db'].get(id, ('id', 'name'))

    if not res:
        raise click.ClickException("Stored value with ID %s not found." % id)

    ctx.obj['db'].delete(id)
    click.echo('-' * 10)
    click.echo("Deleted: (%s) %s" % (res['id'], res['name']))
    click.echo('-' * 10)


@cli.command('wipe', short_help='Deletes all data')
@click.pass_context
def wipe(ctx):
    '''
    Deletes all data
    '''
    if click.confirm('This will delete all data, continue?'):
        click.echo("Deleting data...")
        ctx.obj['db'].wipe()
        click.echo("Done.")
    else:
        click.echo("Cancelled.")


@cli.command('search', short_help='Lists all stored passwords, optionally filtering by name')
@click.argument('name', default='')
@click.pass_context
def search(ctx, name=''):
    '''
    Lists all stored passwords and files, optionally filtering by name.

    NOTE: List is readable by anyone and hence never store any sensitive data in the name field.
    '''

    #click.clear()

    if name:
        click.echo('Search results for: %s' % name)
    else:
        click.echo('Listing stored data.')


    offset = 0

    res = ctx.obj['db'].search(name, limit=PAGE_SIZE, offset=offset * PAGE_SIZE, order_by='name')

    while res:
        click.echo('%s\t%s\t%s\t%s' % ('id', 'name', 'type', 'updated'))

        for r in res:
            click.echo('%s\t%s\t%s\t%s' % (r['id'], r['name'], r['type'], r['updated']))

        if len(res) == PAGE_SIZE:
            click.echo('Press any key to continue with results, or Q to exit')
            v = click.getchar()
            if v.lower() == 'q':
                ctx.exit()
                return

            offset += 1

            res = ctx.obj['db'].search(name, limit=PAGE_SIZE, offset=offset * PAGE_SIZE, order_by='name')

        else:
            break

    # total = 1000
    # click.echo('-' * 15)
    # click.echo('Found %s elements' % total)
    # # testing
    # for i in range(total):
    #     click.echo('Test %s' % i)
    #     if i % PAGE_SIZE == 0 and i > 0:
    #         # click.pause('Press any key to continue with results: %s / %s' % (i + 1, total))

    #         click.echo('Press any key to continue with results, or Q to exit: %s / %s' % (i + 1, total))
    #         v = click.getchar(False)

    #         if v.lower() == 'q':
    #             ctx.exit()
    #             return
