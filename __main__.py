#!/usr/bin/env python

# Entry point to start the CLI interface if called as a scritp directly
# or from a zip file.

from pw_store import cli



if __name__ == '__main__':
    #pylint: disable=E1120
    cli()
